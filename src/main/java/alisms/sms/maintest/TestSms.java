package alisms.sms.maintest;

import java.io.IOException;

import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;

import alisms.sms.util.AliParam;
import alisms.sms.util.AliSendBack;
import alisms.sms.util.AliSmsSend;
import alisms.sms.util.AliSmsUtil;
import alisms.sms.util.SmsTemplate;
import alisms.sms.util.createRandom;

/**
 * 阿里云短信封装测试类
 * @author konglingyinxia
 *
 */
public class TestSms {
	/**
	 * 短信发送测试主方法
	 * @param args
	 */
	public static void main(String[] args) {
		String  tel = "13015582371";//换为自己的电话号码
		String code;
		try {
			code = createRandom.getYZM();
			AliParam ali = SmsTemplate.getAliLogin(code,tel);
			SendSmsResponse yzm = AliSmsSend.getYZM(ali);
			System.out.println("短信接口返回的数据----------------");
	        System.out.println("Code=" + yzm.getCode());
	        System.out.println("Message=" +yzm.getMessage());
	        System.out.println("RequestId=" + yzm.getRequestId());
	        System.out.println("BizId=" + yzm.getBizId());
	        
	        //短信发送返回值
	        AliSendBack aliSendBack = new AliSendBack();
	        aliSendBack.setCode(yzm.getCode());
	        aliSendBack.setBizId(yzm.getBizId());
	        aliSendBack.setMessage(yzm.getMessage());
	        aliSendBack.setRequestId(yzm.getRequestId());
	        
	        
	        
	        
	        
	        //短信明细查询
			if(yzm.getCode() != null && yzm.getCode().equals("OK")){
				 QuerySendDetailsResponse querySendDetailsResponse = AliSmsUtil.querySendDetails(ali,yzm);
		            System.out.println("短信明细查询接口返回数据----------------");
		            System.out.println("Code=" + querySendDetailsResponse.getCode());
		            System.out.println("Message=" + querySendDetailsResponse.getMessage());
		            int i = 0;
		            for(QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs())
		            {
		                System.out.println("SmsSendDetailDTO["+i+"]:");
		                System.out.println("Content=" + smsSendDetailDTO.getContent());
		                System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
		                System.out.println("OutId=" + smsSendDetailDTO.getOutId());
		                System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
		                System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
		                System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
		                System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
		                System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
		            }
		            System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
		            System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e);
		} catch (ClientException e) {
			e.printStackTrace();
			System.out.println(e);
		}
		
	}
}
