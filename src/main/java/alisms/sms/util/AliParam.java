package alisms.sms.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * 短信参数实体类
 * @author 卫星
 *
 */
public class AliParam{
	private static final Log log = LogFactory.getLog(AliParam.class);
	
	private static String accessKeyId;//key
	private static String accessKeySecret;//密钥
	private static String signName;//签名
	private String TemplateCode;//模版Code
	private String TemplateParam;//模版参数
	private String tel;//电话
	private String code;//验证码
	static {
		try{
			InputStream is = AliParam.class.getResourceAsStream("/data.properties");
			Properties properties = new Properties();
			BufferedReader bf = new BufferedReader(new InputStreamReader(is));
			properties.load(bf);
			
			accessKeyId = properties.getProperty("accessKeyId");
			accessKeySecret = properties.getProperty("accessKeySecret");
			signName = properties.getProperty("signName");
		}catch(Exception ex){
			log.debug("加载配置文件："+ex.getMessage());
		}
	}
	public AliParam() {
		super();
	}
	public AliParam(String templateCode, String templateParam, String tel,
			String code) {
		super();
		TemplateCode = templateCode;
		TemplateParam = templateParam;
		this.tel = tel;
		this.code = code;
	}
	public String getAccessKeyId() {
		return accessKeyId;
	}
	public String getAccessKeySecret() {
		return accessKeySecret;
	}
	public String getSignName() {
		return signName;
	}
	public String getTemplateCode() {
		return TemplateCode;
	}
	public void setTemplateCode(String templateCode) {
		TemplateCode = templateCode;
	}
	public String getTemplateParam() {
		return TemplateParam;
	}
	public void setTemplateParam(String templateParam) {
		TemplateParam = templateParam;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public static void main(String[] args) {
		
		System.out.println(new AliParam().getAccessKeyId());
	}
}
