package alisms.sms.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 配置文件读取工具类
 * @author konglingyinxia
 *
 */
public class PropertiesUtil {
		/**
		 * 创建对象
		 */
	    private Properties props=null;
	   
	    public PropertiesUtil() throws IOException{
	        InputStream in = PropertiesUtil.class.getClassLoader().getResourceAsStream("data.properties");
	        props = new Properties();
	        if(in == null){
	        	System.out.println("文件为空");
	        }else{
	        props.load(in);
	        }
	        //关闭资源
	        in.close();
	    }
	    public String readProperties(String name) throws FileNotFoundException,IOException  {
	        //保存所有的键值
	        String Property = "";
	        if(name !=null){
	        	Property = props.getProperty(name);
	        }
	        return Property;
	    }

	    //test
	    public static void main(String[] args) {
	    	System.out.println(PropertiesUtil.class.getResource("").getPath());
	    	System.out.println(PropertiesUtil.class.getResource("/").getPath());
	    	System.out.println(PropertiesUtil.class.getClassLoader().getResource("").getPath());
	    	PropertiesUtil p;
	        try {
	        	System.out.println("---------");
	            p = new PropertiesUtil();
	            System.out.println(p.readProperties("CODE_LENGTH"));
	        } catch (IOException e) {
	            e.printStackTrace();
	            
	        }
	    }
	}
