package alisms.sms.util;

import java.io.IOException;

/**
 * 短信模版读取工具类
 * @author 卫星
 *
 */
public class SmsTemplate {

	/**
	 * 登录验证短信模版
	 * @param tel 
	 * @param code 
	 * @throws IOException 
	 */
	public static AliParam getAliLogin(String code, String tel) throws IOException{
		PropertiesUtil p = new PropertiesUtil();
		
		AliParam ali = new AliParam();
		ali.setCode(code);
		ali.setTel(tel);
		ali.setTemplateCode(p.readProperties("TemplateCode_1"));
		ali.setTemplateParam(p.readProperties("TemplateParam_1"));
		return ali;
		
	}
	
	/**
	 * 注册短信验证模版
	 */
	public static AliParam getAliRgister(String code, String tel) throws IOException{
		PropertiesUtil p = new PropertiesUtil();
		
		AliParam ali = new AliParam();
		ali.setCode(code);
		ali.setTel(tel);
		ali.setTemplateCode(p.readProperties("TemplateCode_2"));
		ali.setTemplateParam(p.readProperties("TemplateParam_2"));
		return ali;
	}
	
}
